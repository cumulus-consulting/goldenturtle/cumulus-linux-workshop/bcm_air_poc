#cloud-config
bootcmd: []
disable_ec2_metadata: true
runcmd:
#- {
#      echo "An error occurred in line $line of $file: exit code '$1' while running: '$2'" 
#      exit 1
#  }
#  trap 'error_handler $? "$BASH_COMMAND"' ERR
- 'echo ''Head node cloud-init: Started.'''
- 'echo ''Head node cloud-init: Setting up eth0.'''
- echo Set head node internal IP
- ip link set up dev eth0
- ip address flush dev eth0
- ip addr add 10.141.255.254/255.255.0.0 dev eth0
- 'echo ''Head node cloud-init: Setting up eth1.'''
- echo Set head node external IP
- ip link set up dev eth1
- ip address flush dev eth1
- dhclient eth1 -v 
- echo 'root:3tango' | chpasswd
- echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB2MlviiF4DuQYtbQc3hUuhTRxBGM7iYPu7FOa30zuFs berkink@nvidia.com' >> /root/.ssh/authorized_keys
- '! grep -P ''^ *AdvancedConfig *='' "/cm/local/apps/cmd/etc/cmd.conf" &&
  echo ''AdvancedConfig = { "VirtualCluster=1" }'' >> "/cm/local/apps/cmd/etc/cmd.conf"'
- '! grep -P ''^ *AdvancedConfig *='' "/cm/images/default-image/cm/local/apps/cmd/etc/cmd.conf"
  && echo ''AdvancedConfig = { "VirtualCluster=1" }'' >> "/cm/images/default-image/cm/local/apps/cmd/etc/cmd.conf"'
- chmod +x /cm/node-installer/pre-initialize-script
- echo Loading modules
- source /etc/profile.d/modules.sh
- module load cluster-tools
- module load cm-setup || true
- 'echo ''Head node cloud-init: Running cm-bright-setup.'''
- cm-bright-setup -c /root/cm/cm-bright-setup.conf --on-error-action abort
- 'echo ''Head node cloud-init: cm-bright-setup complete.'''
- 'echo ''Head node cloud-init: Waiting for CMDaemon to be ready.'''
- chmod +x /root/cm/cmd_ready
- /root/cm/cmd_ready
- 'echo ''Head node cloud-init: CMDaemon is ready.'''
- 'echo ''Head node cloud-init: Complete.'''
write_files:
- content: |
    #!/bin/bash
    date
    echo "Waiting for CMDaemon on `hostname` to update configuration files and start services..."
    if [ "$(curl -m 61 -s -o /dev/null -w %{http_code} http://localhost:8080/ready?wait=60\&version=query)" == "404" ]; then
        echo "The CMDaemon on `hostname` doesn't support the ready service. Will wait for 30 seconds to allow CMDaemon to set up base services."
        sleep 30
        echo "Done, will assume CMDaemon on `hostname` is ready."
        exit 0
    fi
    # CM-42419, We check the ready version of cmd, write it into tmp file.
    # Later the version is used to make decision which endpoint to call
    while [ "$(curl -m 61 -s -o /tmp/ready_version -w %{http_code} http://localhost:8080/ready?wait=60\&version=query)" != "200" ]; do
        date
        echo "CMDaemon on `hostname` is not ready yet. Will wait some more..."
        sleep 3
    done
    if [ "$(cat /tmp/ready_version)" == "ready" ]; then
        curl_url="http://localhost:8080/ready?wait=60&name=RamdiskService&name=SysConfigGenerator&name=SysConfigWriter&name=dhcpd&name=named&name=nfs&name=shorewall&name=nslcd"
    elif [ "$(cat /tmp/ready_version)" == "v2" ]; then
        curl_url="http://localhost:8080/ready?wait=60&name=cod"
    else
        echo "Querying ready service version returned unexpected response: "
        echo "$(cat /tmp/ready_version)"
        exit 1
    fi
    while [ "$(curl -m 61 -s -o /dev/null -w %{http_code} $curl_url)" != "200" ]; do
        date
        echo "Services on `hostname` are not ready yet. Will wait some more..."
        sleep 3
    done
    date
    echo "CMDaemon on `hostname` is ready."
  path: /root/cm/cmd_ready
- content: <?xml version='1.0' encoding='ISO-8859-1'?><!-- This is the CoD
    default --><!-- Just a single xfs partition --><diskSetup xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>  <device>    <blockdev>/dev/vda</blockdev>    <blockdev>/dev/sda</blockdev>    <blockdev>/dev/nvme0n1</blockdev>    <blockdev
    mode="cloud">/dev/oracleoci/oraclevdb</blockdev>    <blockdev mode='cloud'>/dev/sdb</blockdev>    <blockdev
    mode='cloud'>/dev/hdb</blockdev>    <blockdev mode='cloud'>/dev/vdb</blockdev>    <blockdev
    mode='cloud'>/dev/xvdf</blockdev>    <partition id='a2'>      <size>max</size>      <type>linux</type>      <filesystem>xfs</filesystem>      <mountPoint>/</mountPoint>      <mountOptions>defaults,noatime,nodiratime</mountOptions>    </partition>  </device></diskSetup>
  path: /root/cm/node-disk-setup.xml
- content: |
    #!/bin/bash

    # This script is setup by the cluster-on-demand client. It finds disks with a
    # single XFS partition (that's not the node-installer) and grows the file-
    # system to span the entire disk.
    disks=$(lsblk -lno name,type | grep 'disk$' | awk '{print $1}')
    mkdir /tmp/xfs_mount
    for disk in ${disks}; do
      echo -n "Detected disk /dev/${disk}"
      nr_of_partitions=$(lsblk -lno name,type /dev/${disk} | grep -c 'part$')
      if [ "${nr_of_partitions}" -eq "1" ]; then
        partition=$(lsblk -lno name,type /dev/${disk} | grep 'part$' | awk '{print $1}')
        # note, use 'file' instead of 'lsblk'
        # 'lsblk -lno fstype /dev/$partition' sometimes returns empty, see CM-34808
        # "SGI XFS filesystem data" string is from 'file' program source, see CM-41737
        if file --special-files --dereference /dev/$partition | grep --quiet --ignore-case "SGI XFS filesystem data"; then
          if [ "$(lsblk -lno label /dev/${partition})" != "BCMINSTALLER" ]; then
            echo ", resizing partition /dev/${partition}."
            echo "resizepart 1 -1" | parted /dev/${disk}

            echo -e "
    Mounting /dev/${partition}."
            tries=5
            while [[ "${tries}" -gt "0" ]]; do
              # On edge directors, edge iso image is already mounted on /mnt/
              # 2 devices mounted on /mnt, makes xfs_growfs fail. So we use /tmp/xfs_mount
              if ! mount /dev/${partition} /tmp/xfs_mount; then
                ((tries--))
                echo "Mounting /dev/${partition} failed, ${tries} attempts left."
                sleep 1
              else
                echo "Mounted /dev/${partition}."
                break
              fi
            done
            if [[ "${tries}" -eq "0" ]]; then
              echo "Failed to mount /dev/${partition}, aborting."
              exit 1
            fi

            echo "Resizing XFS file-system on /dev/${partition}."
            xfs_growfs /tmp/xfs_mount

            echo "Unmounting /dev/${partition}."
            umount /tmp/xfs_mount
          else
            echo ", partition /dev/${partition} has node-installer label, skipping resize."
          fi
        else
          echo ", partition /dev/${partition} is not XFS, skipping resize."
        fi
      else
        echo ", contains ${nr_of_partitions} partitions, skipping resize."
      fi
    done
  path: /root/cm/category-initialize-script
- content: |
    modules:
      brightsetup:
        bright:
          admin_email: berkink@nvidia.com
          hostname: bcm-nv-air-iso
          license:
            cluster_name: bcm-nv-air-iso
            country: US
            locality: None
            organization: None
            product_key: 145551-362153-716975-162472-876813
            state: None
            unit: None
          master_compute_node: false
          node_count: 1
          node_disk_setup_path: /root/cm/node-disk-setup.xml
          node_kernel_modules:
          - virtio_net
          - virtio_pci
          - virtio_blk
          password: 3tango
          pbspro_lic_server: ''
          timezone: Europe/Amsterdam
          wlm: ''
          wlm_slot_count: AUTO
        cloud_type: openstack
        openstack:
          head_node_internal_ip: 10.141.255.254
          internal_cidr: 10.141.0.0/16
  path: /root/cm/cm-bright-setup.conf